# SCH和PCB库_开源

#### 介绍
此项目为开源SCH库和PCB库，采用Altium Designer软件设计
包括（原理图库、元器件库、3D库）。希望大家都可以来贡献一份自己的力量。让SCH库和PCB库，更完善。

更多PCB封装库请在下面网站下载
https://www.szlcsc.com/
更多3D封装库请在下面网站
http://www.3dcontentcentral.cn/

